package com.employee;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class EmployeeJDBCemplate {
	private DataSource dataSource;
	private JdbcTemplate template;

	public void setDataSource(DataSource dataSource){
		this.dataSource = dataSource;
		this.template = new JdbcTemplate(dataSource);
	}

	public void create(String name, String designation) {
		String sql = "insert into Employee(id,name,designation) values(1,?,?)";
		template.update(sql,name,designation);
		return;
	}

	public List<Employee> getEmployeeList() {
		RowMapper<Employee> employeeMapper = new RowMapper<Employee>() {

			@Override
			public Employee mapRow(ResultSet rs, int rowCount) throws SQLException {
				Employee emp = new Employee();
				emp.setName(rs.getString("name"));
				emp.setDesignation(rs.getString("designation"));
				emp.setSalary(rs.getFloat("salary"));
				return emp;
			}
		};
		
		String sql = "select * from employee";
		List<Employee> empList = template.query(sql, employeeMapper);
		return empList;
	}

}
