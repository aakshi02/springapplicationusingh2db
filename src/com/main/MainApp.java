package com.main;


import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;

import com.employee.Employee;
import com.employee.EmployeeJDBCemplate;
import com.student.Student;
import com.student.StudentJDBCTemplate;

@Controller
public class MainApp {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");

		EmployeeJDBCemplate employeeJDBCemplate = (EmployeeJDBCemplate) context.getBean("employeeJDBCTemplate");
		//employeeJDBCemplate.create("aa", "designation");
		
		List<Employee> empList = employeeJDBCemplate.getEmployeeList();
		empList.stream().forEach(emp -> System.out.println(emp.getName() + " , " + emp.getDesignation()));
		
		StudentJDBCTemplate studentJDBCTemplate = 
				(StudentJDBCTemplate)context.getBean("studentJDBCTemplate");

		System.out.println("------Records Creation--------" );
		studentJDBCTemplate.create("Zara", 11);
		studentJDBCTemplate.create("Nuha", 2);
		studentJDBCTemplate.create("Ayan", 15);

		System.out.println("------Listing Multiple Records--------" );
		List<Student> students = studentJDBCTemplate.listStudents();

		for (Student record : students) {
			System.out.print("ID : " + record.getId() );
			System.out.print(", Name : " + record.getName() );
			System.out.println(", Age : " + record.getAge());
		}

		System.out.println("----Updating Record with ID = 2 -----" );
		studentJDBCTemplate.update(2, 20);

		System.out.println("----Listing Record with ID = 2 -----" );
		Student student = studentJDBCTemplate.getStudent(2);
		System.out.print("ID : " + student.getId() );
		System.out.print(", Name : " + student.getName() );
		System.out.println(", Age : " + student.getAge());
	}
}
