package com.employee;

import java.util.List;

public interface EmployeeDAO {

	public void create(String name, String designation);
	
	public List<Employee> getEmployeeList();
}
